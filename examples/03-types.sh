#!/bin/bash

. $(dirname $(realpath "$0"))/common.sh

cmp "-i 1" "$(cat << EOF
integer: 1
unsigned: 0
long: 0
unsigned long: 0
long long: 0
unsigned long long: 0
float: 0
double: 0
hexadecimal: 0
clamped: 0
state: 
file: 
short_stirng: 
EOF
)"


cmp "-i 1.3" "$(cat << EOF
03-types: invalid value '1.3' for '-i' argument!
EOF
)"

cmp "-i1 -u 3" "$(cat << EOF
integer: 1
unsigned: 3
long: 0
unsigned long: 0
long long: 0
unsigned long long: 0
float: 0
double: 0
hexadecimal: 0
clamped: 0
state: 
file: 
short_stirng: 
EOF
)"

cmp "-u -3" "$(cat << EOF
03-types: invalid value '-3' for '-u' argument!
EOF
)"

cmp "-u 30000000000" "$(cat << EOF
03-types: invalid value '30000000000' for '-u' argument!
EOF
)"

cmp "--long-long 30000000000 -f30000000000 -d 1e-9 --hex a" "$(cat << EOF
integer: 0
unsigned: 0
long: 0
unsigned long: 0
long long: 30000000000
unsigned long long: 0
float: 3e+10
double: 1e-09
hexadecimal: 10
clamped: 0
state: 
file: 
short_stirng: 
EOF
)"

cmp "-c 30" "$(cat << EOF
integer: 0
unsigned: 0
long: 0
unsigned long: 0
long long: 0
unsigned long long: 0
float: 0
double: 0
hexadecimal: 0
clamped: 10
state: 
file: 
short_stirng: 
EOF
)"

cmp "--state two" "$(cat << EOF
integer: 0
unsigned: 0
long: 0
unsigned long: 0
long long: 0
unsigned long long: 0
float: 0
double: 0
hexadecimal: 0
clamped: 0
state: two
file: 
short_stirng: 
EOF
)"

cmp "--file /bin/sh" "$(cat << EOF
integer: 0
unsigned: 0
long: 0
unsigned long: 0
long long: 0
unsigned long long: 0
float: 0
double: 0
hexadecimal: 0
clamped: 0
state: 
file: /bin/sh
short_stirng: 
EOF
)"

cmp "--file ./atata" "$(cat << EOF
The file ./atata (from --file argument) is not readable.
03-types: invalid value './atata' for '--file' argument!
EOF
)"

cmp "--short-string iiiiiiiiiiiiiiii" "$(cat << EOF
03-types: invalid value 'iiiiiiiiiiiiiiii' for '--short-string' argument!
EOF
)"

cmp "--custom-parser 112" "$(cat << EOF
Error message from custom parser
   key = --custom-parser
   value = 112
03-types: invalid value '112' for '--custom-parser' argument!
EOF
)"
