#!/bin/bash

. $(dirname $(realpath "$0"))/common.sh

cmp "-h" "$(cat << EOF
example-04 -- A program for reading and writing enties

Usage:
  04-help [-v]
  04-help [-v] read [FILE...]
  04-help [-v] write [-o | --output FILE]

Note that this text is automatically filled to the width of 72 characters,
but it is possible to force a line-break, for example
<--- here.

Options:
  -v, --verbose                 Produce verbose output.
  -h, --help                    Print help (this message) and exit
  --version                     Print version information and exit

Commands:
  write                         Writes an entry to specified file
  read                          Reads entries from specified files

Environment variables:
  SOME_VAR                      Description for SOME_VAR
  ANOTHER_VAR                   Description for ANOTHER_VAR
  NOT EVEN A VAR                Lorem ipsum dolor sit amet, consetetur
                                sadipscing elitr, sed diam nonumy
                                eirmodtempor invidunt ut labore et
                                dolore magna aliquyam erat, sed diam
                                voluptua.

EOF
)"

cmp "write -h" "$(cat << EOF
example-04 write -- Writes an entry to specified file

Usage:
  04-help write [-v] write [-o | --output FILE]

Options:
  -o, --output FILE             Path to the output file
  -h, --help                    Print help (this message) and exit
  -v, --version                 Print version information and exit
EOF
)"

cmp "write -v" "$(cat << EOF
example-04 write 2.2.1
EOF
)"

cmp "read -h" "$(cat << EOF
example-04 read -- Reads entries from specified files

Options:
  [FILE...]                     Files to read
  -f, --flag-1                  Some flag with no puprpose
  -h, --help                    Print help (this message) and exit
EOF
)"

cmp "--ver" "$(cat << EOF
04-help: unknown argument: '--ver'

Did you mean this:
  -v, --verbose                 Produce verbose output.
  --version                     Print version information and exit
EOF
)"

