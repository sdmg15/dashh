#!/bin/bash

. $(dirname $(realpath "$0"))/common.sh

cmp "-h" "$(cat << EOF
  -a N                          This options is visible
  cmd1                          This options is visible
  -?, -h, --help                Print help (this message) and exit
EOF
)"

cmp "-a1 -b2 cmd1 " "$(cat << EOF
-a: 1
-b: 2
cmd: cmd1
f: 0
g: 0
EOF
)"

cmp "-a1 -b2 -fg cmd1 " "$(cat << EOF
-a: 1
-b: 2
cmd: cmd1
f: 1
g: 1
EOF
)"

