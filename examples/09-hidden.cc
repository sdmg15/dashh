#include <iostream>
#include <vector>

#include "Dashh.hh"

int main(int argc, char const *argv[])
{
	using namespace dashh;

	Command cmd;

	int a = 0;
	int b = 0;
	bool f = false;
	bool g = false;
	std::string action;

	cmd << Option(a, "-a", "N", "This options is visible")
		<< Option(b, "-b", "N", "This options is not visible", Type::Hidden)
		<< Flag(f, "-f", "This flag is not visible", Type::Hidden)
		<< Flag(g, "-g", "This flag is not visible", Type::Hidden)
		<< Command(action, "cmd1", "This options is visible")
		<< Command(action, "cmd2", "This options is not visible", Type::Hidden)
		<< Flag(PageHelp(), "-?, -h, --help", "Print help (this message) and exit");

	Dashh t(&cmd);
	t.parse(argc, argv);

	std::cout << "-a: " << a << "\n";
	std::cout << "-b: " << b << "\n";
	std::cout << "cmd: " << action << "\n";
	std::cout << "f: " << f << "\n";
	std::cout << "g: " << f << "\n";

	return 0;
}

