#include <iostream>
#include <string>

#include "dashh.h"

int main(int argc, const char *argv[])
{
	using namespace dashh;

	Command cmd;

	bool verbose = false;

	cmd << Flag(verbose, "-v, --verbose");

	std::string host;
	cmd << Positional(host, "HOST");

	int port = 0;
	cmd << Option(port, "-p, --port", "PORT");

	Dashh t(&cmd);
	t.parse(argc, argv);

	std::cout << std::boolalpha;
	std::cout << "host: "    << host    << "\n";
	std::cout << "port: "    << port    << "\n";
	std::cout << "verbose: " << verbose << "\n";

	return 0;
}
