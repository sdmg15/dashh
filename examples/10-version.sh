#!/bin/bash

. $(dirname $(realpath "$0"))/common.sh

cmp "-v" "$(cat << EOF
v2.3.1
EOF
)"

cmp "cmd1 -v" "$(cat << EOF
v3.2.1
EOF
)"

cmp "cmd2 -v" "$(cat << EOF
v2.3.1
EOF
)"

cmp "-v cmd1 -v" "$(cat << EOF
v2.3.1
EOF
)"
