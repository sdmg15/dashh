#include <iostream>
#include <vector>

#include "Dashh.hh"

int main(int argc, char const *argv[])
{
	using namespace dashh;

	Command cmd;

	int a = 0;
	cmd << Option(a, "-a", "A");

	int b = 0;
	cmd << Option(b, "-b", "B");

	Dashh t(&cmd);
	t.disablePageParseError();
	t.disablePageUnknownArg();
	t.disablePageMissingValue();

	int nparsed = t.parse(argc, argv);
	if (nparsed < argc) {
		std::cerr << "Can not parse: " << argv[nparsed] << std::endl;
		std::exit(EXIT_FAILURE);
	}

	std::cout << "-a: " << a << std::endl;
	std::cout << "-b: " << b << std::endl;

	return 0;
}

