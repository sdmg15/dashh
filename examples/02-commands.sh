#!/bin/bash

. $(dirname $(realpath "$0"))/common.sh

cmp "-v" "$(cat << EOF
verbose: true
action: 
output: 
input:
EOF
)"

cmp "-v write" "$(cat << EOF
verbose: true
action: write
output: 
input:
EOF
)"

cmp "-v write -o 1" "$(cat << EOF
verbose: true
action: write
output: 1
input:
EOF
)"

cmp "read 1" "$(cat << EOF
verbose: false
action: read
output: 
input: 1
EOF
)"

cmp "read 1 a b c -v" "$(cat << EOF
verbose: false
action: read
output: 
input: 1 a b c -v
EOF
)"

cmp "ls" "$(cat << EOF
verbose: false
action: ls
output: 
input:
EOF
)"

cmp "list" "$(cat << EOF
verbose: false
action: list
output: 
input:
EOF
)"

cmp "index -v" "$(cat << EOF
02-commands ls: unknown argument: '-v'
EOF
)"

