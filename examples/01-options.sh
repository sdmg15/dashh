#!/bin/bash

echo "0:$0"
echo "1:$1"

. $(dirname $(realpath "$0"))/common.sh

cmp "-v" "$(cat << EOF
host: 
port: 0
verbose: true
flag: false
EOF
)"

cmp "-f" "$(cat << EOF
host: 
port: 0
verbose: false
flag: true
EOF
)"

cmp "-fv" "$(cat << EOF
host: 
port: 0
verbose: true
flag: true
EOF
)"

cmp "-p80" "$(cat << EOF
host: 
port: 80
verbose: false
flag: false
EOF
)"

cmp "-p 80" "$(cat << EOF
host: 
port: 80
verbose: false
flag: false
EOF
)"

cmp "--port 80" "$(cat << EOF
host: 
port: 80
verbose: false
flag: false
EOF
)"

cmp "--port=80" "$(cat << EOF
host: 
port: 80
verbose: false
flag: false
EOF
)"

cmp "-vf -p 80" "$(cat << EOF
host: 
port: 80
verbose: true
flag: true
EOF
)"

cmp "-p" "$(cat << EOF
01-options: option '-p' requires a value!
EOF
)"

cmp "-p localhost" "$(cat << EOF
01-options: invalid value 'localhost' for '-p' argument!
EOF
)"

cmp "-v localhost -p 80" "$(cat << EOF
host: localhost
port: 80
verbose: true
flag: false
EOF
)"


