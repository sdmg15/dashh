#!/bin/bash

. $(dirname $(realpath "$0"))/common.sh

cmp "-b 1 -b 0 -b true --bool-list FALSE" "$(cat << EOF
-b, --bool-list: 1 0 1 0 
-s, --string-list: 
-i, --int-list: 
-f, --float-list: 
-c, --custom-list: 
EOF
)"

cmp "-s 1 -i 0 -si --int-list=23 -f 2 -f -3" "$(cat << EOF
-b, --bool-list: 
-s, --string-list: 1 i 
-i, --int-list: 0 23 
-f, --float-list: 2 -3 
-c, --custom-list: 
EOF
)"

cmp "-c 12 -c3 -csdf" "$(cat << EOF
-b, --bool-list: 
-s, --string-list: 
-i, --int-list: 
-f, --float-list: 
-c, --custom-list: 12 3 sdf 
EOF
)"

cmp "-c 12 -c000000000000000 -csdf" "$(cat << EOF
05-multiple: invalid value '000000000000000' for '-c000000000000000' argument!
EOF
)"
