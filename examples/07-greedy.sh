#!/bin/bash

. $(dirname $(realpath "$0"))/common.sh

cmp "-- 123 -a 23 --value" "$(cat << EOF
--: 123 -a 23 --value 
--args: 
ARGS...: 
EOF
)"

cmp "- 123 -a 23 --value" "$(cat << EOF
--: 123 -a 23 --value 
--args: 
ARGS...: 
EOF
)"

cmp "--" "$(cat << EOF
07-greedy: option '--' requires a value!
EOF
)"

cmp "-a 123 -a 23 --value" "$(cat << EOF
--: 
--args: 123 -a 23 --value 
ARGS...: 
EOF
)"

cmp "123 -a 23 --value" "$(cat << EOF
--: 
--args: 
ARGS...: 123 -a 23 --value 
EOF
)"

