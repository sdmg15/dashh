#!/bin/bash

. $(dirname $(realpath "$0"))/common.sh

cmp "-b 1,0,true,FALSE -s abc:sd,a -b 0 -s1,1" "$(cat << EOF
-b, --bool-list: 1 0 1 0 0 
-s, --string-list: abc:sd a 1 1 
-i, --int-list: 
-f, --float-list: 
-c, --custom-list: 
EOF
)"

cmp "-f 4:2;12,2:3.01" "$(cat << EOF
-b, --bool-list: 
-s, --string-list: 
-i, --int-list: 
-f, --float-list: 4 2 12 2 3.01 
-c, --custom-list: 
EOF
)"

cmp "-c aaa,bbb -c ccc --custom-list=ddd" "$(cat << EOF
-b, --bool-list: 
-s, --string-list: 
-i, --int-list: 
-f, --float-list: 
-c, --custom-list: aaa bbb ccc ddd 
EOF
)"

