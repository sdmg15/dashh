#include "String.hh"

#include "Argument.hh"

namespace dashh
{

String::String(std::string &output)
: m_output(output)
{ }

String::~String()
{ }

Parser::Result String::parse(
	const std::string &,
	const std::string &value,
	const Argument *
) const
{
	m_output = value;
	return Parser::Parsed;
}

Parser *String::clone() const
{
	return new String(*this);
}

} /* dashh */ 
