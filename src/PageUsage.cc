#include <cassert>
#include <cstdio>

#include "Argument.hh"
#include "Flag.hh"
#include "Option.hh"
#include "Positional.hh"
#include "Command.hh"
#include "PageUsage.hh"
#include "Utils.hh"

namespace dashh
{
PageUsage::PageUsage()
{ }

PageUsage::~PageUsage()
{ }

void PageUsage::add(const Command *command)
{
	m_usage = command->usage();
}

void PageUsage::add(const Flag *flag)
{
	if (flag->parent())
		flag->parent()->dispatch(this);
}

void PageUsage::add(const Option *option)
{
	if (option->parent())
		option->parent()->dispatch(this);
}

void PageUsage::add(const Positional *positional)
{
	if (positional->parent())
		positional->parent()->dispatch(this);
}

Page *PageUsage::clone() const
{
	return new PageUsage(*this);
}

void PageUsage::build(
		const Argument *argument,
		const std::vector<std::string> &trace
) { 
	assert(!trace.empty());
	m_cmd = Utils::trace_to_path(trace);

	argument->dispatch(this);
}

void PageUsage::print(const std::string &, const std::string &)
{
	if (m_usage.empty())
		m_usage.push_back("...");

	fprintf(stdout, "Usage:\n");
	for (auto &u : m_usage)
		fprintf(stdout, "  %s %s\n", m_cmd.c_str(), u.c_str());
}

void PageUsage::exit() const
{
	std::exit(EXIT_SUCCESS);
}

} /*  dashh */ 
