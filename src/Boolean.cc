#include "Boolean.hh"

#include <algorithm>
#include <vector>

#include "Argument.hh"

namespace dashh
{

Boolean::Boolean(bool &output)
: m_output(output)
{ }

Boolean::~Boolean()
{ }

Parser::Result Boolean::parse(
	const std::string &,
	const std::string &value,
	const Argument *
) const {
	using namespace std;

	std::string vlower = value;
	transform(vlower.begin(), vlower.end(), vlower.begin(), (int (*)(int)) std::tolower);

	const static vector<string> yes({"1", "true", "t", "y", "yes", "on"});
	const static vector<string> no({"0", "false", "f", "n", "no", "off"});

	if (find(yes.begin(), yes.end(), vlower) != yes.end()) {
		m_output = true;
		return Parser::Parsed;
	} else if (find(no.begin(), no.end(), vlower) != no.end()) {
		m_output = false;
		return Parser::Parsed;
	} else {
		return Parser::Error;
	}
}

Parser *Boolean::clone() const
{
	return new Boolean(*this);
}

} /* dashh */ 
