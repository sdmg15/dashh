#include "Text.hh"
#include "Utils.hh"
#include "Dashh.hh"
#include "Page.hh"

namespace dashh
{

Text::Text(
	const std::string &text,
	size_t indent,
	size_t width
)
: m_text(text)
, m_indent(indent)
, m_width(width)
{ }

Text::~Text()
{ }

TreeNode *Text::clone() const
{
	return new Text(*this);
}

TreeNode *Text::move()
{
	return new Text(std::move(*this));
}

void Text::dispatch(Page *page) const
{
	page->add(this);
}

void Text::dispatch(Dashh *grammar) const
{
	grammar->add(this);
}

const std::string &Text::text() const
{
	return m_text;
}

size_t Text::indent() const
{
	return m_indent;
}

size_t Text::width() const
{
	return m_width;
}

} /* dashh */ 
