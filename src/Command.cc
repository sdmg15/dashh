#include "Command.hh"

#include "Dashh.hh"
#include "Page.hh"
#include "Utils.hh"
#include "Text.hh"
#include "KeyValue.hh"

namespace dashh
{

Command::Command()
: Argument()
{ }

Command::Command(const Command &other)
: Argument(other)
, m_version(other.m_version)
, m_usage(other.m_usage)
{
	for (TreeNode *n = child(); n; n = n->sibling())
		m_children.push_back(n);
}

Command::Command(Command &&other) noexcept
: Argument(std::move(other))
, m_version(std::move(other.m_version))
, m_usage(std::move(other.m_usage))
, m_children(std::move(other.m_children))
{ }

Command::Command(
	const Parser &parser,
	const std::string &names,
	const std::string &description,
	Type type
)
: Argument(parser, names, "", description, type)
{ }

Command::Command(
	const Page &page,
	const std::string &names,
	const std::string &description,
	Type type
)
: Argument(page, names, "", description, type)
{ }

Command::~Command()
{
	for (auto &n : m_children)
		delete n;
}

TreeNode *Command::clone() const
{
	return new Command(*this);
}

TreeNode *Command::move()
{
	return new Command(std::move(*this));
}

Command &Command::operator<<(const TreeNode &node)
{
	auto c = node.clone();
	m_children.push_back(c);
	add(c);

	return *this;
}

Command &Command::operator<<(TreeNode &node)
{
	auto c = node.move();
	m_children.push_back(c);
	add(c);

	return *this;
}

Command &Command::operator<<(const std::string &str)
{
	TreeNode *t = new Text(str);
	m_children.push_back(t);
	add(t);

	return *this;
}

void Command::dispatch(Page *page) const
{
	page->add(this);
}

void Command::dispatch(Dashh *grammar) const
{
	grammar->add(this);
}

void Command::setDescription(const std::string &description)
{
	m_description = description;
}

void Command::setVersion(const std::string &version)
{
	m_version = version;
}

const std::vector<std::string> &Command::usage() const
{
	return m_usage;
}

const std::string Command::version() const
{
	return m_version;
}

} /* dashh */ 
