#include "Parser.hh"
#include "Option.hh"
#include "Utils.hh"
#include "Page.hh"
#include "Dashh.hh"

namespace dashh
{

Option::Option(
	const Parser &parser,
	const std::string &names,
	const std::string &placeholder,
	const std::string &description,
	Type type
)
: Argument(parser, names, placeholder, description, type)
{ }

Option::Option(
	const Page &page,
	const std::string &names,
	const std::string &placeholder,
	const std::string &description,
	Type type
)
: Argument(page, names, placeholder, description, type)
{ }

Option::~Option()
{ }

TreeNode *Option::clone() const
{
	return new Option(*this);
}

TreeNode *Option::move()
{
	return new Option(std::move(*this));
}

void Option::dispatch(Page *page) const
{
	page->add(this);
}

void Option::dispatch(Dashh *grammar) const
{
	grammar->add(this);
}

} /* dashh */ 
