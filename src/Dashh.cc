#include <cassert>
#include <stdexcept>
#include <iostream>

#include "Page.hh"
#include "Argument.hh"
#include "Dashh.hh"
#include "Command.hh"
#include "Flag.hh"
#include "Positional.hh"
#include "Option.hh"
#include "Context.hh"

#include "Utils.hh"

namespace dashh
{

const std::string Dashh::DEFAULT_TRUE = "true";
const std::string Dashh::DEFAULT_FALSE = "false";

template <typename T>
T findByName(const std::string &name, std::vector<T> args)
{
	for (auto &c : args) {
		auto &v = c->names();
		if (std::find(v.cbegin(), v.cend(), name) != v.cend())
			return c;
	}

	return nullptr;
}

Dashh::Dashh(const Command *tree)
: m_pageParseError(nullptr)
, m_pageUnknownArg(nullptr)
, m_pageMissingValue(nullptr)
, m_pageMissingRequired(nullptr)
, m_disableParseError(false)
, m_disableUnknownArg(false)
, m_disableMissingValue(false)
, m_disableMissingRequired(false)
, m_root(tree)
{
	for (TreeNode *a = m_root->child(); a; a = a->sibling()) {
		a->dispatch(this);
	}
}

Dashh::~Dashh()
{ }

void Dashh::add(const Command *command)
{
	checkRedefinitions(command->names());
	m_commands.push_back(command);
}

void Dashh::add(const Flag *flag)
{
	checkRedefinitions(flag->names());
	m_flags.push_back(flag);
}

void Dashh::add(const Positional *positional)
{
	m_positionals.push_back(positional);
}

void Dashh::add(const Option *option)
{
	checkRedefinitions(option->names());
	m_options.push_back(option);
}

void Dashh::add(const KeyValue *)
{ }

void Dashh::add(const Text *)
{ }

void Dashh::add(const Usage *)
{ }

void Dashh::setPageParseError(Page *page)
{
	m_pageParseError = page;
}

void Dashh::setPageUnknownArg(Page *page)
{
	m_pageUnknownArg = page;
}

void Dashh::setPageMissingValue(Page *page)
{
	m_pageMissingValue = page;
}

void Dashh::setPageMissingRequired(Page *page)
{
	m_pageMissingRequired = page;
}

void Dashh::disablePageParseError(bool disable)
{
	m_disableParseError = disable;
}

void Dashh::disablePageUnknownArg(bool disable)
{
	m_disableUnknownArg = disable;
}

void Dashh::disablePageMissingValue(bool disable)
{
	m_disableMissingValue = disable;
}

void Dashh::disablePageMissingRequired(bool disable)
{
	m_disableMissingRequired = disable;
}

void Dashh::showParseError(
	const Argument *argument,
	const std::string &key,
	const std::string &val
) const
{
	if (m_disableParseError)
		return;

	PageParseError page;
	Page *s = m_pageParseError ? m_pageParseError : &page;

	s->build(argument, m_trace);
	s->print(key, val);
	s->exit();
}

void Dashh::showUnknownArg(
	const Argument *argument,
	const std::string &key
) const
{
	if (m_disableUnknownArg)
		return;

	PageUnknownArg page;
	Page *s = m_pageUnknownArg ? m_pageUnknownArg : &page;

	s->build(argument, m_trace);
	s->print(key);
	s->exit();
}

void Dashh::showMissingValue(
	const Argument *argument,
	const std::string &key
) const
{
	if (m_disableMissingValue)
		return;

	PageMissingValue page;
	Page *s = m_pageMissingValue ? m_pageMissingValue : &page;

	s->build(argument, m_trace);
	s->print(key);
	s->exit();
}

void Dashh::showMissingRequired(const Argument *argument) const
{
	if (m_disableMissingRequired)
		return;

	PageMissingRequired page;
	Page *s = m_pageMissingRequired ? m_pageMissingRequired : &page;

	s->build(argument, m_trace);
	s->print();
	s->exit();
}

size_t Dashh::parse(int argc, char const *argv[])
{
	Context ctx(argc, argv);

	parse(ctx);

	return ctx.position();
}

void Dashh::parse(Context &ctx)
{
	parseRoot(ctx);

	parseArgs(ctx);

	parseCommands(ctx);

	if (ctx.end())
		return;

	showUnknownArg(m_root, ctx.current());
	ctx.finilize();
}

void Dashh::parseRoot(Context &ctx)
{
	if (ctx.end())
		return;

	auto key = ctx.current();
	if (!m_root->names().empty())
		key = m_root->names()[0];

	m_trace.push_back(key);

	do {
		auto val = ctx.current();

		if (m_root->parse(key, val, m_trace) == Parser::Error) {
			showParseError(m_root, key, val);
			ctx.finilize();
		}

		ctx.advance();
	} while (!ctx.end() && m_root->type().greedy());

	m_parsed.push_back(m_root);
}

void Dashh::parseArgs(Context &ctx)
{
	for (; !ctx.end(); ctx.advance()) {
		if (parseSingleFlags(ctx))
			continue;

		if (parseJoinedFlags(ctx))
			continue;

		if (parseSingleOptions(ctx))
			continue;

		if (parseJoinedOptions(ctx))
			continue;

		if (parseOptions(ctx))
			continue;

		if (parsePositionals(ctx))
			continue;

		break;
	} 

	auto r = findMissingRequired();
	if (!r)
		return;

	showMissingRequired(r);
	ctx.finilize();
}

bool Dashh::parseSingleFlags(Context &ctx)
{
	if (ctx.end())
		return false;

	auto key = ctx.current();

	auto flag = findByName(key, m_flags);
	if (!flag)
		return false;

	if (flag->parse(key, DEFAULT_TRUE, m_trace) == Parser::Error) {
		showParseError(flag, key, DEFAULT_TRUE);
		ctx.finilize();
		return false;
	}

	m_parsed.push_back(flag);

	return true;
}

bool Dashh::parseJoinedFlags(Context &ctx)
{
	if (ctx.end())
		return false;

	auto key = ctx.current();
	if (!areJoinedFlags(key))
		return false;

	auto splitted = splitJoinedFlags(key);

	for (auto &s : splitted) {
		if (!findByName(s, m_flags))
			return false;
	}

	for (auto &s : splitted) {
		auto flag = findByName(s, m_flags);
		assert(flag);

		if (flag->parse(s, DEFAULT_TRUE, m_trace) == Parser::Error) {
			showParseError(flag, key, DEFAULT_TRUE);
			ctx.finilize();
			return false;
		}

		m_parsed.push_back(flag);
	}

	return true;
}

bool Dashh::parseSingleOptions(Context &ctx)
{
	if (ctx.end())
		return false;

	auto key = ctx.current();

	if (!isSingleOption(key))
		return false;


	auto kv = splitSingleOption(key);

	auto option = findByName(kv.first, m_options);
	if (!option)
		return false;

	if (option->parse(kv.first, kv.second, m_trace) == Parser::Error) {
		showParseError(option, key, kv.second);
		ctx.finilize();
		return false;
	}

	m_parsed.push_back(option);

	return true;
}

bool Dashh::parseJoinedOptions(Context &ctx)
{
	if (ctx.end())
		return false;

	auto key = ctx.current();

	if (!isJoinedOption(key))
		return false;

	auto kv = splitJoinedOption(key);

	auto option = findByName(kv.first, m_options);
	if (!option)
		return false;

	if (option->parse(kv.first, kv.second, m_trace) == Parser::Error) {
		showParseError(option, key, kv.second);
		ctx.finilize();
		return false;
	}

	m_parsed.push_back(option);

	return true;
}

bool Dashh::parseOptions(Context &ctx)
{
	if (ctx.end())
		return false;

	auto key = ctx.current();

	auto option = findByName(key, m_options);
	if (!option)
		return false;

	if (ctx.last()) {
		showMissingValue(option, key);
		ctx.finilize();
		return false;
	}

	do {
		auto val = ctx.next();

		if (option->parse(key, val, m_trace) == Parser::Error) {
			showParseError(option, key, val);
			ctx.finilize();
			return false;
		}

		ctx.advance();

	} while (option->type().greedy() && !ctx.last());

	m_parsed.push_back(option);

	return true;
}

bool Dashh::parsePositionals(Context &ctx)
{
	if (ctx.end())
		return false;

	const Positional *positional = nullptr;
	for (auto &o : m_positionals) {
		bool parsed = Utils::found(static_cast<const Argument *>(o), m_parsed);
		if (parsed && !o->type().greedy() && !o->type().multiple())
			continue;

		positional = o;
		break;
	}

	if (!positional)
		return false;

	auto key = positional->placeholder();

	while (!ctx.end()) {
		auto val = ctx.current();

		if (positional->parse(key, val, m_trace) == Parser::Error) {
			showParseError(positional, key, val);
			ctx.finilize();
			return false;
		}

		if (!positional->type().greedy())
			break;

		ctx.advance();
	}

	m_parsed.push_back(positional);

	return true;
}

void Dashh::parseCommands(Context &ctx)
{
	if (ctx.end())
		return;

	auto key = ctx.current();
	const Command *command = findByName(key, m_commands);

	if (!command)
		return;

	Dashh g(command);
	g.m_pageParseError = m_pageParseError;
	g.m_pageUnknownArg = m_pageUnknownArg;
	g.m_pageMissingValue = m_pageMissingValue;
	g.m_pageMissingRequired = m_pageMissingRequired;
	g.m_trace = m_trace;
	g.parse(ctx);
}

const Argument *Dashh::findMissingRequired()
{
	for (auto &o : m_options) {
		if (!o->type().required())
			continue;

		if (!Utils::found(static_cast<const Argument *> (o), m_parsed))
			return o;
	}

	for (auto &o : m_positionals) {
		if (!o->type().required())
			continue;

		if (!Utils::found(static_cast<const Argument *> (o), m_parsed))
			return o;
	}

	return nullptr;
}

bool Dashh::areJoinedFlags(const std::string &s)
{
	if (s.length() < 3)
		return false;

	if (s[0] != '-')
		return false;

	if (s[1] == '-')
		return false;

	return true;
}

bool Dashh::isSingleOption(const std::string &s)
{
	if (s.length() < 4)
		return false;

	if (s[0] != '-' || s[1] != '-')
		return false;

	for (size_t i = 2; i < s.length(); ++i)
		if (s[i] == '=')
			return true;

	return false;
}

bool Dashh::isJoinedOption(const std::string &s)
{
	if (s.length() < 3)
		return false;

	if (s[0] != '-')
		return false;

	if (s[1] == '-')
		return false;

	return true;
}

std::vector<std::string> Dashh::splitJoinedFlags(
	const std::string &s
)
{
	assert(areJoinedFlags(s));

	std::vector<std::string> ret;

	for (size_t i = 1; i < s.length(); ++i)
		ret.push_back(std::string("-") + s[i]);

	return ret;
}

std::pair<std::string, std::string> Dashh::splitSingleOption(
	const std::string &s
)
{
	assert(isSingleOption(s));

	size_t eq = 2;
	for (eq = 2; eq < s.length(); ++eq) {
		if (s[eq] == '=')
			break;
	}

	std::string key = s.substr(0, eq);
	std::string val = s.substr(eq + 1, std::string::npos);

	return {key, val};
}

std::pair<std::string, std::string> Dashh::splitJoinedOption(
	const std::string &s
) 
{
	assert(isJoinedOption(s));

	std::string key = s.substr(0, 2);
	std::string val = s.substr(2, std::string::npos);

	return {key, val};
}

void Dashh::checkRedefinitions(const std::vector<std::string> &names) const
{
	for (auto &n : names) {
		if (
			findByName(n, m_flags) == nullptr &&
			findByName(n, m_options) == nullptr &&
			findByName(n, m_commands) == nullptr
		) {
			continue;
		}

		std::string m = n + " is already defined!\n";
		throw std::invalid_argument(m);
	}
}

} /* dashh */ 
