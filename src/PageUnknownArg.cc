#include <cassert>
#include <cmath>
#include <queue>
#include <iostream>

#include "Argument.hh"
#include "Flag.hh"
#include "Option.hh"
#include "Command.hh"
#include "PageUnknownArg.hh"

#include "Utils.hh"

namespace dashh
{

PageUnknownArg::PageUnknownArg()
{ }

PageUnknownArg::~PageUnknownArg()
{ }

Page *PageUnknownArg::clone() const
{
	return new PageUnknownArg(*this);
}

void PageUnknownArg::add(const Command *command)
{
	// First function call is from parent command
	static bool is_parent = true;

	if (is_parent) {
		is_parent = false;

		for (auto child = command->child(); child; child = child->sibling())
			child->dispatch(this);

		return;
	}

	// Following function calls is from children subcommands
	if (!command->type().hidden())
		m_alternatives.push_back({command, pattern(command)});
}

void PageUnknownArg::add(const Flag *flag)
{
	if (!flag->type().hidden())
		m_alternatives.push_back({flag, pattern(flag)});
}

void PageUnknownArg::add(const Option *option)
{
	if (!option->type().hidden())
		m_alternatives.push_back({option, pattern(option)});
}

void PageUnknownArg::add(const Positional *)
{ }

void PageUnknownArg::add(const Text *)
{ }

void PageUnknownArg::add(const Usage *)
{ }

void PageUnknownArg::add(const KeyValue *)
{ }

void PageUnknownArg::build(
	const Argument *argument,
	const std::vector<std::string> &trace
) { 
	assert(!trace.empty());
	m_cmd = Utils::trace_to_path(trace);

	assert(argument);
	argument->dispatch(this);
}

struct suggestion_t {
	double dist;
	std::string pattern;
	std::string description;
};

bool operator <(const suggestion_t &a, const suggestion_t &b)
{
	return a.dist > b.dist;
}

void PageUnknownArg::print(const std::string &key, const std::string &)
{
	std::cerr << m_cmd << ": unknown argument: \'" << key << "\'\n";

	std::priority_queue<suggestion_t> m_suggestions;

	for (const auto &alternative : m_alternatives) {
		double min_dist = std::numeric_limits<double>::max();

		const Argument *argument;
		std::string pattern;
		std::tie(argument, pattern) = alternative;

		for (auto &name : argument->names()) {
			double dist = typoDistance(name, key);
			if (dist < min_dist)
				min_dist = dist;
		}

		if (min_dist > m_maxDistance)
			continue;

		m_suggestions.push({min_dist, pattern, argument->description()});
	}

	if (m_suggestions.size() == 0)
		return;

	std::cerr << "\nDid you mean this:" << std::endl;

	size_t i = 0;
	while (!m_suggestions.empty() && i < m_maxSuggestions) {
		auto s = m_suggestions.top();
		std::cerr << fit(s.pattern, s.description, column1Indent, column1Width, column2Indent, column2Width);

		m_suggestions.pop();
		i++;
	}
}

void PageUnknownArg::exit() const
{
	std::exit(EXIT_FAILURE);
}

static constexpr size_t kb_x = 12;
static constexpr size_t kb_y = 5;

static const char lower_kb[kb_y][kb_x] = {
	{'1'  , '2'  , '3' , '4' , '5' , '6' , '7' , '8' , '9' , '0'  , '-'   , '='}  ,
	{'q'  , 'w'  , 'e' , 'r' , 't' , 'y' , 'u' , 'i' , 'o' , 'p'  , '['   , ']'}  ,
	{'a'  , 's'  , 'd' , 'f' , 'g' , 'h' , 'j' , 'k' , 'l' , ';'  , '\''  , '|'}  ,
	{'z'  , 'x'  , 'c' , 'v' , 'b' , 'n' , 'm' , ',' , '.' , '/'  , '\0'  , '\0'} ,
	{'\0' , '\0' , ' ' , ' ' , ' ' , ' ' , ' ' , ' ' , ' ' , '\0' , '\0'  , '\0'}
};

static const char upper_kb[kb_y][kb_x] = {
	{'!'  , '@'  , '#' , '$' , '%' , '^' , '&' , '*' , '(' , ')'  , '-'  , '+'}  ,
	{'Q'  , 'W'  , 'E' , 'R' , 'T' , 'Y' , 'U' , 'I' , 'O' , 'P'  , '{'  , '}'}  ,
	{'A'  , 'S'  , 'D' , 'F' , 'G' , 'H' , 'J' , 'K' , 'L' , ':'  , '"'  , '|'}  ,
	{'Z'  , 'X'  , 'C' , 'V' , 'B' , 'N' , 'M' , '<' , '>' , '?'  , '\0' , '\0'} ,
	{'\0' , '\0' , ' ' , ' ' , ' ' , ' ' , ' ' , ' ' , ' ' , '\0' , '\0' , '\0'}
};

std::tuple<size_t, size_t, bool> PageUnknownArg::position(char ch)
{
	for (size_t i = 0; i < kb_y; ++i) {
		for (size_t j = 0; j < kb_x; ++j) {
			if (upper_kb[i][j] == ch)
				return std::make_tuple(i, j, true);
			if (lower_kb[i][j] == ch)
				return std::make_tuple(i, j, false);
		}
	}

	return std::make_tuple(0, 0, false);
}

double PageUnknownArg::keyboardDistance(char a, char b)
{
	size_t ai, aj, bi, bj;
	bool aupper, bupper;
	std::tie(ai, aj, aupper) = position(a);
	std::tie(bi, bj, bupper) = position(b);

	double p1 = static_cast<double>(ai) - bi;
	double p2 = static_cast<double>(aj) - bj;
	double d = std::sqrt(p1*p1 + p2*p2);

	if (aupper != bupper)
		d += m_shiftCost;

	return d;
}

double PageUnknownArg::typoDistance(const std::string &a, const std::string &b)
{
	size_t m = a.size();
	size_t n = b.size();

	if (m == 0)
		return n;
	if (n == 0)
		return m;

	std::vector< std::vector<double> > dp(m + 1);
	for (size_t i = 0; i <= m; ++i)
		dp[i].resize(n + 1);

	for (size_t i = 0; i <= m; ++i) {
		dp[i][0] = 0;
		for (size_t j = 0; j < i; ++j)
			dp[i][0] += m_deletionCost;
	}

	for (size_t j = 0; j <= n; ++j) {
		dp[0][j] = 0;
		for (size_t i = 0; i < j; ++i)
			dp[0][j] += m_insertionCost;
	}

	for (size_t j = 1; j <= n; ++j) {
		for (size_t i = 1; i <= m; ++i) {
			if (a[i - 1] == b[j - 1]) {
				dp[i][j] = dp[i - 1][j - 1];
				continue;
			} 

			double del = m_deletionCost;
			double ins = (i == a.length() ? m_insertionCost : keyboardDistance(a[i], b[j - 1]));
			double sub = keyboardDistance(a[i - 1], b[j - 1]);

			dp[i][j] = std::min(
				dp[i - 1][j] + del,
				std::min(
					dp[i][j - 1] + ins,
					dp[i - 1][j - 1] + sub
				)
			);
		}
	}

	return dp[m][n];
}

} /*  dashh */ 
