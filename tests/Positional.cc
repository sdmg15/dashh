#include "gtest/gtest.h"

#include "mocks/ParserMock.hh"
#include "mocks/ArgumentMock.hh"
#include "mocks/PageMock.hh"

#include "Positional.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

ParserMock parser;

TEST(ctor, no_description) {
	Positional o(parser, "ph");
	EXPECT_TRUE(o.description().empty());
}

TEST(ctor, no_type) {
	Positional o(parser, "ph");
	EXPECT_TRUE(o.type().none());
}

TEST(ctor, placeholder_is_set) {
	Positional o(parser, "ph");
	EXPECT_EQ(o.placeholder(), "ph");
}

TEST(ctor, description_is_set) {
	Positional o(parser, "ph", "des");
	EXPECT_EQ(o.description(), "des");
}

TEST(ctor, type_is_set) {
	Positional o(parser, "ph", "des", Type::Hidden);
	EXPECT_EQ(o.type(), Type::Hidden);
}

TEST(dispatch_page, scree_add_called) {
	PageMock s;
	Positional o(parser, "ph");

	EXPECT_CALL(s, add(&o));

	o.dispatch(&s);

}
