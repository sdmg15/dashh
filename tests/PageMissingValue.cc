#include "gtest/gtest.h"

#include "Command.hh"
#include "Flag.hh"
#include "Option.hh"
#include "Dashh.hh"
#include "PageMissingValue.hh"
#include "mocks/ParserMock.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

TEST(pageparseerror, call_from_option) {
	Option opt(ParserMock(), "-a");

	PageMissingValue sv;
	sv.build(&opt, {"argv_0", "cmd"});

	testing::internal::CaptureStderr();

	sv.print("-a");

	string output = testing::internal::GetCapturedStderr();

	EXPECT_EQ(output, "argv_0 cmd: option \'-a\' requires a value!\n");

	EXPECT_EXIT({sv.exit();}, ExitedWithCode(1), ".*");
}

