#include "gtest/gtest.h"

#include "String.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

TEST(parse, output_is_stored) {
	string output;
	String parser(output);

	Parser::Result result = parser.parse("key", "value", nullptr);

	EXPECT_EQ(output, "value");
	EXPECT_EQ(result, Parser::Parsed);
}

TEST(clone, output_is_stored_thru_clone) {
	string output;
	String parser(output);

	Parser *p = parser.clone();

	Parser::Result result = p->parse("key", "value", nullptr);

	EXPECT_EQ(output, "value");
	EXPECT_EQ(result, Parser::Parsed);

	delete p;
}

