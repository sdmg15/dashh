#include "gtest/gtest.h"

#include "Lambda.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

TEST(parse, output_is_stored_v) {
	string value;

	Lambda parser([&value](const std::string &v) -> bool {
		value = v;
		return true;
	});

	Parser::Result result = parser.parse("key", "value", nullptr);

	EXPECT_EQ(value, "value");
	EXPECT_EQ(result, Parser::Parsed);
}

TEST(parse, output_is_stored_kv) {
	string key;
	string value;

	Lambda parser([&key, &value](const std::string &k, const std::string &v) -> bool {
		key = k;
		value = v;
		return true;
	});

	Parser::Result result = parser.parse("key", "value", nullptr);

	EXPECT_EQ(key, "key");
	EXPECT_EQ(value, "value");
	EXPECT_EQ(result, Parser::Parsed);
}

TEST(parse, output_is_stored_kva) {
	string key;
	string value;
	const Argument *arg = nullptr;

	Lambda parser([&key, &value, &arg] (
		const std::string &k,
		const std::string &v,
		const Argument *a
	) -> bool {
		key = k;
		value= v;
		arg = a;
		return true;
	});

	Parser::Result result = parser.parse("key", "value", reinterpret_cast<const Argument *>(2));

	EXPECT_EQ(key, "key");
	EXPECT_EQ(value, "value");
	EXPECT_EQ(arg, reinterpret_cast<const Argument *>(2));
	EXPECT_EQ(result, Parser::Parsed);
}

TEST(parse, output_is_stored_v_fail) {
	string value;

	Lambda parser([&value](const std::string &v) -> bool {
		value = v;
		return false;
	});

	Parser::Result result = parser.parse("key", "value", nullptr);

	EXPECT_EQ(value, "value");
	EXPECT_EQ(result, Parser::Error);
}

TEST(parse, output_is_stored_kv_fail) {
	string key;
	string value;

	Lambda parser([&key, &value](const std::string &k, const std::string &v) -> bool {
		key = k;
		value = v;
		return false;
	});

	Parser::Result result = parser.parse("key", "value", nullptr);

	EXPECT_EQ(key, "key");
	EXPECT_EQ(value, "value");
	EXPECT_EQ(result, Parser::Error);
}

TEST(parse, output_is_stored_kva_fail) {
	string key;
	string value;
	const Argument *arg = nullptr;

	Lambda parser([&key, &value, &arg] (
		const std::string &k,
		const std::string &v,
		const Argument *a
	) -> bool {
		key = k;
		value= v;
		arg = a;
		return false;
	});

	Parser::Result result = parser.parse("key", "value", reinterpret_cast<const Argument *>(2));

	EXPECT_EQ(key, "key");
	EXPECT_EQ(value, "value");
	EXPECT_EQ(arg, reinterpret_cast<const Argument *>(2));
	EXPECT_EQ(result, Parser::Error);
}

TEST(clone, output_is_stored_thru_clone) {
	string key;
	string value;
	const Argument *arg = nullptr;

	Lambda parser([&key, &value, &arg] (
		const std::string &k,
		const std::string &v,
		const Argument *a
	) -> bool {
		key = k;
		value= v;
		arg = a;
		return false;
	});

	Parser *p = parser.clone();

	Parser::Result result = p->parse("key", "value", reinterpret_cast<const Argument *>(2));

	EXPECT_EQ(key, "key");
	EXPECT_EQ(value, "value");
	EXPECT_EQ(arg, reinterpret_cast<const Argument *>(2));
	EXPECT_EQ(result, Parser::Error);

	delete p;
}

