#include "gtest/gtest.h"

#include "Command.hh"
#include "Text.hh"
#include "KeyValue.hh"
#include "Flag.hh"
#include "Usage.hh"
#include "Option.hh"
#include "Positional.hh"
#include "Dashh.hh"
#include "PageHelp.hh"
#include "mocks/ParserMock.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

TEST(pagehelp, long_stuff) {
	Command root;

	root << Text("Headline", 25);
	root << Text(
		"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod"
		"tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At"
		"vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,"
		"no sea takimata sanctus est Lorem ipsum dolor sit amet.", 32, 40
	);
	root << "";

	root << Text("Usage:");
	root << Usage("aaaa bbbb cccc");
	root << Usage("xxxx yyyy zzzz");

	root << "";

	root << Text("Options:");
	root << Flag(PageHelp(), "-h", "Print this help", Type::Hidden);
	root << Flag(ParserMock(), "--really-long-options, --that-do-not-fit", "Print this help");
	root << Option(ParserMock(), "--really-long-option-with-ph", "PLACEHOLDER", "Print this help");
	root << Flag(ParserMock(), "--not-so-long", 
		"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod"
		"tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At"
		"vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren"
	);
	root << Flag(ParserMock(), "--another-long, --with-long-description",
		"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod"
		"tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua."
	);

	root << "";

	root << 
		"     Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod"
		"tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At"
		"vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,"
		"no sea takimata sanctus est Lorem ipsum dolor sit amet."
		"\n\n"
		"Lorem ipsum dolor";

	root << "";

	root << "Key values";
	root << KeyValue("key1", "value1", 1, 5, 4, 60);
	root << KeyValue("key2", "value2", 1, 5, 4, 60);
	root << KeyValue("key3", "value3", 1, 5, 4, 60);
	root << KeyValue("key4", 
		"Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod"
		"tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.",
		1, 5, 4, 60
	);

	testing::internal::CaptureStdout();

	Dashh t(&root);
	const char *argv[] = {"argv_0", "-h"};

	EXPECT_EXIT({t.parse(2, argv);}, ExitedWithCode(0), ".*");

	string output = testing::internal::GetCapturedStdout();

	string expected = 
R"(                         Headline
                                Lorem ipsum dolor sit amet, consetetur
                                sadipscing elitr, sed diam nonumy
                                eirmodtempor invidunt ut labore et
                                dolore magna aliquyam erat, sed diam
                                voluptua. Atvero eos et accusam et
                                justo duo dolores et ea rebum. Stet
                                clita kasd gubergren,no sea takimata
                                sanctus est Lorem ipsum dolor sit
                                amet.

Usage:
  argv_0 aaaa bbbb cccc
  argv_0 xxxx yyyy zzzz

Options:
  --really-long-options, --that-do-not-fit
                                Print this help
  --really-long-option-with-ph PLACEHOLDER
                                Print this help
  --not-so-long                 Lorem ipsum dolor sit amet, consetetur
                                sadipscing elitr, sed diam nonumy
                                eirmodtempor invidunt ut labore et
                                dolore magna aliquyam erat, sed diam
                                voluptua. Atvero eos et accusam et
                                justo duo dolores et ea rebum. Stet
                                clita kasd gubergren
  --another-long, --with-long-description
                                Lorem ipsum dolor sit amet, consetetur
                                sadipscing elitr, sed diam nonumy
                                eirmodtempor invidunt ut labore et
                                dolore magna aliquyam erat, sed diam
                                voluptua.

     Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
nonumy eirmodtempor invidunt ut labore et dolore magna aliquyam erat,
sed diam voluptua. Atvero eos et accusam et justo duo dolores et ea
rebum. Stet clita kasd gubergren,no sea takimata sanctus est Lorem ipsum
dolor sit amet.

Lorem ipsum dolor

Key values
 key1     value1
 key2     value2
 key3     value3
 key4     Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
          diam nonumy eirmodtempor invidunt ut labore et dolore
          magna aliquyam erat, sed diam voluptua.
)";

	std::cout << output;

	EXPECT_EQ(output, expected);
}

TEST(pagehelp, args_patterns) {
	Command root;

	root << "Flags:";
	root << Flag(PageHelp(), "-h", "Print this help");
	root << Flag(ParserMock(), "-a, --long-a | -b", "Long");
	root << Flag(ParserMock(), "-, --, -?", "Special cases flags");
	root << "\nOptions:";
	root << Option(ParserMock(), "-x", "", "Hidden option", Type::Hidden);
	root << Option(ParserMock(), "-c", "", "Option without PH");
	root << Option(ParserMock(), "-d, -f", "PLH", "Option with PH");
	root << Option(ParserMock(), "-g, --long-g", "g", "Option with PH");
	root << "\nPositionals:";
	root << Positional(ParserMock(), "OP0",  "Hiddent Positional", Type::Hidden);
	root << Positional(ParserMock(), "OP1",  "Positional");
	root << Positional(ParserMock(), "OP2",  "Required operand", Type::Required);
	root << Positional(ParserMock(), "OP3",  "Positional with Multiple values", Type::Multiple);
	root << Positional(ParserMock(), "OP4",  "Positional with Multiple and Required values", Type::Multiple | Type::Required);
	root << "\nCommands:";
	root << Command(ParserMock(), "cmd0",  "Hidden subcommand", Type::Hidden);
	root << Command(ParserMock(), "cmd1",  "Subcommand 1");
	root << Command(ParserMock(), "cmd2,cmd3|cmd5",  "Subcommand 2");

	testing::internal::CaptureStdout();

	Dashh t(&root);
	const char *argv[] = {"argv_0", "-h"};

	EXPECT_EXIT({t.parse(2, argv);}, ExitedWithCode(0), ".*");

	string output = testing::internal::GetCapturedStdout();

	string expected = 
R"(Flags:
  -h                            Print this help
  -a, --long-a, -b              Long
  -, --, -?                     Special cases flags

Options:
  -c VALUE                      Option without PH
  -d, -f PLH                    Option with PH
  -g, --long-g g                Option with PH

Positionals:
  [OP1]                         Positional
  OP2                           Required operand
  [OP3...]                      Positional with Multiple values
  OP4 [OP4...]                  Positional with Multiple and Required values

Commands:
  cmd1                          Subcommand 1
  cmd2, cmd3, cmd5              Subcommand 2
)";

	// std::cout << output;

	EXPECT_EQ(output, expected);
}

