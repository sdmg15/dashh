#include "gtest/gtest.h"

#include "Argument.hh"

#include "mocks/ParserMock.hh"
#include "mocks/PageMock.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

class ArgumentTest : public Argument
{
public:
	ArgumentTest()
	: Argument()
	{ }

	ArgumentTest(const ArgumentTest &other)
	: Argument(other)
	, name(other.name)
	{ }

	ArgumentTest(ArgumentTest &&other)
	: Argument(std::move(other))
	, name(other.name)
	{ }

	ArgumentTest(const std::string &name)
	: Argument(ParserMock(), "test_name", "test_ph", "test_des", Type::Greedy)
	, name(name)
	{  };

	// TODO: wtf is it for?
	std::string name;

	virtual ~ArgumentTest() {};

	virtual Argument *clone() const {
		return new ArgumentTest(*this);
	}

	virtual Argument *move(){
		return new ArgumentTest(std::move(*this));
	}

	MOCK_CONST_METHOD1(dispatch, void(Page *page));
	MOCK_CONST_METHOD1(dispatch, void(Dashh *grammar));
};

TEST(ctor, name_is_set) {
	ArgumentTest a("aa");

	ASSERT_EQ(a.names().size(), 1u);
	EXPECT_EQ(a.names()[0], "test_name");
}

TEST(ctor, placeholder_is_set) {
	ArgumentTest a("aa");

	EXPECT_EQ(a.placeholder(), "test_ph");
}

TEST(ctor, description_is_set) {
	ArgumentTest a("aa");

	EXPECT_EQ(a.description(), "test_des");
}

TEST(ctor, type_is_set) {
	ArgumentTest a("aa");
	EXPECT_EQ(a.type(), Type::Greedy);
}

TEST(parse, no_parse__retruns_parsed) {
	ArgumentTest a;
	EXPECT_EQ(a.parse("a", "a", {""}), Parser::Parsed);
}
