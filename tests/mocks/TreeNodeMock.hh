#pragma once

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "TreeNode.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

class TreeNodeMock : public TreeNode
{
public:
	TreeNodeMock(const std::string &name = "")
	: name(name)
	{ };

	TreeNodeMock(const TreeNodeMock &other)
	: TreeNode(other)
	, name(other.name)
	{ };

	TreeNodeMock(TreeNodeMock &&other)
	: TreeNode(std::move(other))
	, name(std::move(other.name))
	{ };

	virtual ~TreeNodeMock() {};

	MOCK_CONST_METHOD1(dispatch, void(Page *));
	MOCK_CONST_METHOD1(dispatch, void(Dashh *));

	TreeNode *clone() const
	{
		return new TreeNodeMock(*this);
	}

	TreeNode *move()
	{
		return new TreeNodeMock(std::move(*this));
	}

	std::string name;
};
