#pragma once

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "Argument.hh"
#include "ParserMock.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

class ArgumentMock : public Argument
{
public:
	ArgumentMock(const ArgumentMock &other)
	: Argument(other)
	, name(other.name)
	{ };

	ArgumentMock(ArgumentMock &&other)
	: Argument(std::move(other))
	, name(std::move(other.name))
	{ };

	ArgumentMock(const std::string &name = "")
	: Argument(ParserMock(), "mock_name", "mock_ph", "mock_des", Type::Greedy)
	, name(name)
	{  };

	ArgumentMock(const Parser &parser, const std::string &name = "")
	: Argument(parser, "mock_name", "mock_ph", "mock_des", Type::Greedy)
	, name(name)
	{  };

	std::string name;

	virtual ~ArgumentMock() {};

	virtual Argument *clone() const {
		return new ArgumentMock(*this);
	}

	virtual Argument *move() {
		return new ArgumentMock(std::move(*this));
	}

	MOCK_CONST_METHOD1(dispatch, void(Page *page));
	MOCK_CONST_METHOD1(dispatch, void(Dashh *grammar));
};
