#include "gtest/gtest.h"

#include "Command.hh"
#include "Flag.hh"
#include "Option.hh"
#include "Dashh.hh"
#include "PageVersion.hh"
#include "mocks/ParserMock.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

TEST(pageversion, call_from_root) {
	Command root;
	root.setVersion("v2");

	PageVersion sv;
	sv.build(&root, {"argv_0"});

	testing::internal::CaptureStdout();

	sv.print();

	string output = testing::internal::GetCapturedStdout();

	EXPECT_EQ(output, "v2\n");

	EXPECT_EXIT({sv.exit();}, ExitedWithCode(0), ".*");
}

TEST(pageversion, call_from_flag_parser) {
	Command root;
	root.setVersion("v2");

	Command cmd(ParserMock(), "cmd");
	cmd << Flag(PageVersion(), "-v");

	root << cmd;

	testing::internal::CaptureStdout();

	Dashh t(&root);
	const char *argv[] = {"argv_0", "cmd", "-v"};

	EXPECT_EXIT({t.parse(3, argv);}, ExitedWithCode(0), ".*");

	string output = testing::internal::GetCapturedStdout();
	
	EXPECT_EQ(output, "v2\n");
}

TEST(pageversion, call_from_option_parser) {
	Command root;
	root.setVersion("v2");

	Command cmd(ParserMock(), "cmd");
	cmd << Option(PageVersion(), "-v");

	root << cmd;

	testing::internal::CaptureStdout();

	Dashh t(&root);
	const char *argv[] = {"argv_0", "cmd", "-v1"};

	EXPECT_EXIT({t.parse(3, argv);}, ExitedWithCode(0), ".*");

	string output = testing::internal::GetCapturedStdout();
	
	EXPECT_EQ(output, "v2\n");
}
