#include "gtest/gtest.h"

#include "mocks/ParserMock.hh"
#include "mocks/ArgumentMock.hh"
#include "mocks/PageMock.hh"

#include "Flag.hh"

using namespace std;
using namespace ::testing;
using namespace dashh;

ParserMock parser;

TEST(ctor, no_description__empty_description) {
	Flag f(parser, "p");
	EXPECT_TRUE(f.description().empty());
}

TEST(ctor, no_type__type_is_none) {
	Flag f(parser, "p");
	EXPECT_TRUE(f.type().none());
}

TEST(ctor, description_is_set) {
	Flag f(parser, "p", "d");
	EXPECT_EQ(f.description(), "d");
}

TEST(ctor, type_is_set) {
	Flag f(parser, "p", "d", Type::Hidden);
	EXPECT_EQ(f.type(), Type::Hidden);
}

TEST(ctor, single_name) {
	Flag f(parser, "p");
	ASSERT_EQ(f.names().size(), 1u);
	EXPECT_EQ(f.names()[0], "p");
}

TEST(ctor, multiple_names) {
	Flag f(parser, "p,q");
	ASSERT_EQ(f.names().size(), 2u);
	EXPECT_EQ(f.names()[0], "p");
	EXPECT_EQ(f.names()[1], "q");
}

TEST(dispatch_page, page_add_called) {
	PageMock s;
	Flag f(parser, "p,q");

	EXPECT_CALL(s, add(&f));

	f.dispatch(&s);
}
