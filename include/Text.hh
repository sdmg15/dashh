#ifndef TEXT_HH_18586
#define TEXT_HH_18586

#include <string>

#include "TreeNode.hh"

namespace dashh
{

class Text : public TreeNode
{
public:
	Text(const Text &other) = default;

	Text(Text &&other) = default;

	Text(
		const std::string &text,
		size_t indent = 0,
		size_t width = 74
	);

	virtual ~Text();

	virtual TreeNode *clone() const;

	virtual TreeNode *move();

	virtual void dispatch(Page *page) const;

	virtual void dispatch(Dashh *grammar) const;

	const std::string &text() const;
	size_t indent() const;
	size_t width() const;

protected:
	std::string m_text;
	size_t m_indent;
	size_t m_width;

private:

};

} /* dashh */ 

#endif /* end of include guard: TEXT_HH_18586_HH */
