#ifndef KEYVALUE_HH_95123
#define KEYVALUE_HH_95123

#include <string>

#include "TreeNode.hh"

namespace dashh
{

class KeyValue : public TreeNode
{
public:
	KeyValue(const KeyValue &other) = default;

	KeyValue(KeyValue &&other) = default;

	KeyValue(
		const std::string &col1,
		const std::string &col2,
		size_t keyIndent = 2,
		size_t keyWidth  = 28,
		size_t valIndent = 2,
		size_t valWidth  = 40
	);

	virtual ~KeyValue();

	virtual TreeNode *clone() const;

	virtual TreeNode *move();

	virtual void dispatch(Page *page) const;

	virtual void dispatch(Dashh *grammar) const;

	const std::string &col1() const;
	const std::string &col2() const;
	size_t keyIndent() const;
	size_t valIndent() const;
	size_t keyWidth() const;
	size_t valWidth() const;

protected:
	std::string m_col1;
	std::string m_col2;
	size_t m_keyIndent;
	size_t m_valIndent;
	size_t m_keyWidth;
	size_t m_valWidth;

private:

};

} /* dashh */ 

#endif /* end of include guard: KEYVALUE_HH_95123_HH */
