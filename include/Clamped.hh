#ifndef CLAMPED_HH_6313
#define CLAMPED_HH_6313

#include "Parser.hh"

#include <string>
#include <vector>

namespace dashh
{

template <typename T>
class Clamped : public Parser
{
public:
	using value_type = T;

	Clamped(T &output, T from, T to, int base = 10);

	virtual ~Clamped();

	Parser::Result parse(
		const std::string &key,
		const std::string &value,
		const Argument *argument
	) const;

	Parser *clone() const;

private:
	int m_base;
	T &m_output;
	T m_from;
	T m_to;
};

template <typename T>
Clamped<T> as_clamped(T &output, T from, T to, int base = 10) {
	return Clamped<T>(output, from, to, base);
}

template <typename T>
Clamped<T>::Clamped(T &output, T from, T to, int base)
: m_base(base)
, m_output(output)
, m_from(from)
, m_to(to)
{
	static_assert(std::is_integral<T>::value || std::is_floating_point<T>::value,
		"Specified type T for dashh::Clamped is not supported");
}

template <typename T>
Clamped<T>::~Clamped()
{  }

template <typename T>
Parser::Result Clamped<T>::parse(
	const std::string &,
	const std::string &value,
	const Argument *
) const
{
	using namespace::std;

	try {
		size_t pos = 0;

		if (std::is_same<T, int>::value)
			m_output = stoi(value, &pos, m_base);
		else if (std::is_same<T, long>::value)
			m_output = stol(value, &pos, m_base);
		else if (std::is_same<T, unsigned long>::value)
			m_output = stoul(value, &pos, m_base);
		else if (std::is_same<T, long long>::value)
			m_output = stoll(value, &pos, m_base);
		else if (std::is_same<T, unsigned long long>::value)
			m_output = stoull(value, &pos, m_base);
		else if (std::is_same<T, float>::value)
			m_output = stof(value, &pos);
		else if (std::is_same<T, double>::value)
			m_output = stod(value, &pos);
		else if (std::is_same<T, long double>::value)
			m_output = stold(value, &pos);
		else
			throw std::invalid_argument("Specified type T for dashh::Clamped is not supported");

		if (pos != value.length())
			return Parser::Error;
	} catch(std::logic_error const &) {
		return Parser::Error;
	}

	if (m_output < m_from)
		m_output = m_from;
	if (m_output > m_to)
		m_output = m_to;

	return Parser::Parsed;
}

template <typename T>
Parser *Clamped<T>::clone() const
{
	return new Clamped<T>(*this);
}

} /* dashh */ 

#endif /* end of include guard: CLAMPED_HH_6313_HH */
