#ifndef PAGEPARSEERROR_HH_43458
#define PAGEPARSEERROR_HH_43458

#include "Page.hh"

namespace dashh
{

class Command;
class Flag;
class Positional;
class Option;
class Text;

class PageParseError : public Page
{
public:
	PageParseError();

	virtual ~PageParseError();

	virtual Page *clone() const;

	virtual void build(
		const Argument *argument,
		const std::vector<std::string> &trace
	);

	virtual void print(
		const std::string &key = std::string(),
		const std::string &value = std::string()
	);

	virtual void exit() const;

private:
	std::string m_cmd;
};

} /* dashh */ 

#endif /* end of include guard: PAGEPARSEERROR_HH_43458_HH */
