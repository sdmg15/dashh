#ifndef STRING_HH_10354
#define STRING_HH_10354

#include "Parser.hh"

#include <string>
#include <vector>

namespace dashh
{

class String : public Parser
{
public:
	using value_type = std::string;

	String(std::string &output);
	virtual ~String();

	Parser::Result parse(
		const std::string &key,
		const std::string &value,
		const Argument *argument
	) const;

	Parser *clone() const;

private:
	std::string &m_output;
};

} /* dashh */ 

#endif /* end of include guard: STRING_HH_10354_HH */
