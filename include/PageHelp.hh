#ifndef PAGEHELP_HH_58274
#define PAGEHELP_HH_58274

#include "Page.hh"

namespace dashh
{

class Command;
class Flag;
class Positional;
class Option;
class Text;

class PageHelp : public Page
{
public:
	PageHelp();

	virtual ~PageHelp();

	virtual Page *clone() const;

	virtual void add(const Command *command);
	virtual void add(const Flag *flag);
	virtual void add(const Positional *positional);
	virtual void add(const Option *option);
	virtual void add(const Text *text);
	virtual void add(const Usage *text);
	virtual void add(const KeyValue *columns);

	virtual void build(
		const Argument *argument,
		const std::vector<std::string> &trace
	);

	virtual void print(
		const std::string &key = std::string(),
		const std::string &value = std::string()
	);

	virtual void exit() const;

private:
	std::string m_cmd;

	std::vector<std::string> m_usage;
	std::string m_body;

	bool m_have_subcommands;

	bool m_have_arguments;
	bool m_have_required_arguments;
};
} /* dashh */ 

#endif /* end of include guard: PAGEHELP_HH_58274_HH */
