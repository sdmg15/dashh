#ifndef BOOLEAN_HH_2443
#define BOOLEAN_HH_2443

#include "Parser.hh"

#include <string>
#include <vector>

namespace dashh
{

class Boolean : public Parser
{
public:
	using value_type = bool;

	Boolean(bool &output);

	virtual ~Boolean();

	Parser::Result parse(
		const std::string &key,
		const std::string &value,
		const Argument *argument
	) const;

	Parser *clone() const;

private:
	bool &m_output;
};

} /* dashh */ 

#endif /* end of include guard: BOOLEAN_HH_2443_HH */
