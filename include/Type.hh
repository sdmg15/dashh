#ifndef TYPE_HH_61311
#define TYPE_HH_61311

namespace dashh
{

class Type
{
public:
	enum type_e {
		None     = 0,
		Required = (1 << 0),
		Multiple = (1 << 1),
		Hidden   = (1 << 2),
		Greedy   = (1 << 3)
	};

	Type(type_e type = Type::None);

	virtual ~Type ();

	bool none() const;
	bool required() const;
	bool multiple() const;
	bool hidden() const;
	bool greedy() const;

	Type operator| (const Type &other) const;

	bool operator==(const Type &other) const;

	Type(unsigned long type = 0);
private:

	type_e m_type;
};

} /* dashh */ 

#endif /* end of include guard: TYPE_HH_61311_HH */
