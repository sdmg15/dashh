#ifndef USAGE_HH_76458
#define USAGE_HH_76458

#include <string>

#include "TreeNode.hh"

namespace dashh
{

class Page;
class Dashh;

class Usage : public TreeNode
{
public:
	Usage(const Usage &other) = default;

	Usage(Usage &&other) = default;

	Usage(const std::string &usage);

	virtual ~Usage();

	virtual TreeNode *clone() const;

	virtual TreeNode *move();

	virtual void dispatch(Page *page) const;

	virtual void dispatch(Dashh *grammar) const;

	std::string get() const;

protected:
	std::string m_usage;

private:

};

} /* dashh */ 

#endif /* end of include guard: USAGE_HH_76458_HH */
