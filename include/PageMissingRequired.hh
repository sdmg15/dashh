#ifndef PAGEMISSINGREQUIRED_HH_51068
#define PAGEMISSINGREQUIRED_HH_51068

#include "Page.hh"

namespace dashh
{

class Command;
class Flag;
class Positional;
class Option;
class Text;
class Usage;
class KeyValue;

class PageMissingRequired : public Page
{
public:
	PageMissingRequired();

	virtual ~PageMissingRequired();

	virtual Page *clone() const;

	virtual void add(const Command *command);
	virtual void add(const Flag *flag);
	virtual void add(const Positional *positional);
	virtual void add(const Option *option);
	virtual void add(const Text *text);
	virtual void add(const Usage *text);
	virtual void add(const KeyValue *columns);

	virtual void build(
		const Argument *argument,
		const std::vector<std::string> &trace
	);

	virtual void print(
		const std::string &key = std::string(),
		const std::string &value = std::string()
	);

	virtual void exit() const;

private:
	std::string m_cmd;
	std::string m_missing;
};

} /* dashh */ 

#endif /* end of include guard: PAGEMISSINGREQUIRED_HH_51068_HH */
