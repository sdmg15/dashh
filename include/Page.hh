#ifndef PAGE_HH_34525
#define PAGE_HH_34525

#include <string>
#include <vector>

namespace dashh
{

class Argument;
class Command;
class Flag;
class Positional;
class Option;
class Text;
class KeyValue;
class Usage;

class Page
{
public:
	Page();
	virtual ~Page();

	virtual void add(const Command *command);
	virtual void add(const Flag *flag);
	virtual void add(const Positional *positional);
	virtual void add(const Option *option);
	virtual void add(const Text *text);
	virtual void add(const Usage *text);
	virtual void add(const KeyValue *columns);

	virtual Page *clone() const = 0;

	virtual void build(
		const Argument *argument,
		const std::vector<std::string> &trace
	);

	virtual void print(
		const std::string &key = std::string(),
		const std::string &value = std::string()
	);

	virtual void exit() const;

protected:
	const size_t textWidth     = 70;
	const size_t textIndent    = 2;
	const size_t column1Indent = 2;
	const size_t column1Width  = 28;
	const size_t column2Indent = 2;
	const size_t column2Width  = 40;

	static std::string wrap(const std::string &text, size_t width);

	virtual std::string fit(
		const std::string &col1,
		const std::string &col2,
		size_t keyIndent,
		size_t keyWidth,
		size_t valIndent,
		size_t valWidth
	) const;

	virtual std::string pattern(const Command *command) const;
	virtual std::string pattern(const Flag *flag) const;
	virtual std::string pattern(const Positional *positional) const;
	virtual std::string pattern(const Option *option) const;
};

} /* dashh */ 

#endif /* end of include guard: PAGE_HH_34525_HH */
